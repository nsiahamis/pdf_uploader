pipeline {

    agent { label 'will' }

    parameters {
        choice(name: 'SITE', choices: ['bringin', 'convergence', 'heir2020', 'ibidaas', 'new3aces', 'sentinel', 'smartship', 'teaching'], description: 'Choose the project folder in which you want to upload the files.')
        stashedFile 'FILETOUPLOAD'
        string(name: 'fileName', description: "Give the file's name with extension (e.g. BringIn-EU-2020.pdf)")
    }

    stages {

        stage("Upload file to Will") {
            steps{
                withFileParameter('FILETOUPLOAD'){
                    sh """
                    cp $FILETOUPLOAD /var/itmlsites/www/$SITE/sites/default/files/docs/$fileName
                    sudo chown 33:33 /var/itmlsites/www/$SITE/sites/default/files/docs/$fileName
                    """
                }
            }
        }

    }

    post {
        always{
            cleanWs deleteDirs: true
        }
        success {
            sh """
            echo "Upload finished successfully!"
            """
        }
    }

}